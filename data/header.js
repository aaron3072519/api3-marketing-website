export const data = {
  menu: [
    {
      title: 'For Developers',
      navItems: [
        {
          label: 'Data feeds',
          description:
            'Power your dApp with the most secure data feeds in DeFi.',
          link: '/data-feeds',
          icon: '',
        },
        {
          label: 'QRNG',
          description: 'Quantum Random Numbers for Smart Contracts.',
          link: '/qrng',
          icon: '',
        },
        {
          label: 'Web3 APIs',
          description: 'Build data-rich smart contracts with world-class APIs.',
          link: '/web3-apis',
          icon: '',
        },
      ],
    },
    {
      title: 'For data providers',
      navItems: [
        {
          label: 'Airnode',
          description: 'Placeholder description',
          link: '/airnode',
          icon: '',
        },
        {
          label: 'API3 Alliance',
          description: 'Placeholder description',
          link: '/api3-alliance',
          icon: '',
        },
      ],
    },
    {
      title: 'DAO',
      navItems: [
        {
          label: 'DAO Dashboard',
          description: 'Placeholder description',
          link: 'https://api3.eth.limo/',
          icon: '',
          isExternal: true,
        },
        {
          label: 'DAO Tracker',
          description: 'Placeholder description',
          link: 'https://tracker.api3.org/',
          icon: '',
          isExternal: true,
        },
      ],
    },
    {
      title: 'Resources',
      navItems: [
        {
          label: 'DOCs',
          link: 'https://docs.api3.org',
          icon: '',
          isExternal: true,
        },
        {
          label: 'Articles',
          link: 'https://medium.com/api3',
          icon: '',
          isExternal: true,
        },
        {
          label: 'Whitepapers',
          icon: '',
          subcategories: [
            {
              label: 'English',
              link: 'https://drive.google.com/file/d/1b8QsGPCJJC1pQOcg83-knD1IAOIgCtZZ/view',
              icon: '../src/assets/svg/pdf-icon.svg',
              isExternal: true,
            },
            {
              label: 'Chinese | 中文',
              link: 'https://drive.google.com/file/d/1f2Kad5CUgbr7plyGBtWnK-Shfm3XowWy/view',
              icon: '../src/assets/svg/pdf-icon.svg',
              isExternal: true,
            },
            {
              label: 'Korean | 한국어',
              link: 'https://drive.google.com/file/d/1JMHwdNibJf5g_bBaG8Sx9cCg5_14ZIvE/view',
              icon: '../src/assets/svg/pdf-icon.svg',
              isExternal: true,
            },
            {
              label: 'Spanish | Español',
              link: 'https://drive.google.com/file/d/1C_HEQ41TBE-W2IKqMZTaVR9pND6hes8S/view',
              icon: '../src/assets/svg/pdf-icon.svg',
              isExternal: true,
            },
            {
              label: 'Vietnamese | Tiếng Việt',
              link: 'https://drive.google.com/file/d/1ELcBjb8nO_4S4jhhZnFMeSo2BHrznONj/view',
              icon: '../src/assets/svg/pdf-icon.svg',
              isExternal: true,
            },
          ],
        },
      ],
    },
    {
      title: 'Participate',
      navItems: [
        {
          label: 'GitHub',
          description: 'Placeholder description',
          link: 'https://github.com/API3DAO',
          icon: '',
          isExternal: true,
        },
        {
          label: 'Open Bank Project',
          description: 'Placeholder description',
          link: '/open-bank-project',
          icon: '',
        },
        {
          label: 'Open positions',
          description: 'Placeholder description',
          link: '/open-positions',
          icon: '',
        },
        {
          label: 'Contact',
          description: 'Placeholder description',
          icon: '',
          subcategories: [
            {
              label: 'Join API3 Discord',
              link: 'https://discord.com/invite/qnRrcfnm5W',
              icon: '',
              isExternal: true,
            },
            {
              label: 'Visit API3 Forum',
              link: 'https://forum.api3.org',
              icon: '',
              isExternal: true,
            },
            {
              label: 'Support Email',
              link: 'mailto:help@api3.org',
              icon: '',
            },
            {
              label: 'General Email',
              link: 'mailto:contact@api3.org',
              icon: '',
            },
          ],
        },
      ],
    },
  ],
}
