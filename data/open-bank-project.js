export const data = {
  meta: {
    title: 'Open Bank Project',
    description: '',
  },

  hero: {
    heading: 'Open Banking on the Blockchain',
    text: 'Rapidly build blockchain-banking products and services with Airnode-enabled open banking APIs.',
    color: 'blue',
    button: {
      label: 'Register your Interest',
      link: '',
    },
  },

  project: {
    heading: 'API3 x Open Bank Project',
    text: [
      'The Open Bank Project is a partner of API3. It provides developers with access to more than 400 APIs for open banking platforms. Airnode, from API3, gives blockchain solutions access to these APIs',
    ],
    color: 'emerald',
    button: {
      label: 'Learn more',
      link: '',
      color: 'green',
    },
    image: '',
  },

  features: [
    {
      heading: 'Open Banking APIs for Web 3.0',
      text: 'Connect to over 400 APIs used by many of the world’s leading financial institutions for open banking data such as:',
      list: [
        'Onboarding and KYC',
        'Accounts',
        'Transactions',
        'Counterparties',
        'Branches, ATMs',
        'Metadata',
      ],
      image: '',
      button: {
        label: 'Register your Interest',
        link: '',
      },
    },
    {
      heading: 'Airnode for Open Banking',
      text: '',
      list: [
        'Expose open banking APIs to blockchain-based solutions in a fully API provider-controlled manner, without third-party middlemen.',
        'Leverage open banking data for new and innovative products and services on Web 3.0.',
        'Build connections between digital banking services and growing blockchain use cases like decentralized finance (DeFi) and self-sovereign identities (SSI).',
        'Create distributed database solutions for verification and authorization, powered by blockchain-enabled open banking APIs.',
      ],
      image: '',
      button: {
        label: 'Learn more',
        link: '',
        color: 'blue',
      },
    },
  ],

  partners: {
    heading: 'API3 Partners',
    text: 'Organizations who participate in the Web3 API economy.',
    color: 'blue',
    logos: [
      {
        name: 'A',
        image: '../src/assets/svg/logoipsum1.svg',
      },
      {
        name: 'B',
        image: '../src/assets/svg/logoipsum2.svg',
      },
      {
        name: 'C',
        image: '../src/assets/svg/logoipsum3.svg',
      },
      {
        name: 'D',
        image: '../src/assets/svg/logoipsum4.svg',
      },
      {
        name: 'E',
        image: '../src/assets/svg/logoipsum5.svg',
      },
      {
        name: 'F',
        image: '../src/assets/svg/logoipsum6.svg',
      },
      {
        name: 'A',
        image: '../src/assets/svg/logoipsum1.svg',
      },
      {
        name: 'B',
        image: '../src/assets/svg/logoipsum2.svg',
      },
      {
        name: 'C',
        image: '../src/assets/svg/logoipsum3.svg',
      },
      {
        name: 'D',
        image: '../src/assets/svg/logoipsum4.svg',
      },
      {
        name: 'E',
        image: '../src/assets/svg/logoipsum5.svg',
      },
      {
        name: 'F',
        image: '../src/assets/svg/logoipsum6.svg',
      },
    ],
  },
}
