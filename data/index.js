import arbitrum from '../src/assets/images/arbitrum.png'
import avalanche from '../src/assets/images/avalanche.png'
import ethereum from '../src/assets/images/ethereum.png'
import fantom from '../src/assets/images/fantom.png'
import gnosis from '../src/assets/images/gnosis.png'
import moonbeam from '../src/assets/images/moonbeam.png'
import optimism from '../src/assets/images/optimism.png'
import polygon from '../src/assets/images/polygon.png'
import dataFeeds from '../src/assets/svg/icons/data-feeds.svg'
import qrng from '../src/assets/svg/icons/qrng.svg'
import placeholder from '../src/assets/svg/icons/oev-placeholder.svg'
import airnode from '../src/assets/svg/icons/airnode.svg'
import chainapi from '../src/assets/svg/icons/chain-api.svg'
import web3Apis from '../src/assets/svg/icons/web3-apis.svg'
import oevIntroduction from '../src/assets/svg/icons/oev-introduction.svg'
import oevLitepaper from '../src/assets/svg/icons/oev-litepaper.svg'
import logoipsum1 from '../src/assets/svg/logoipsum1.svg'
import logoipsum2 from '../src/assets/svg/logoipsum2.svg'
import logoipsum3 from '../src/assets/svg/logoipsum3.svg'
import logoipsum4 from '../src/assets/svg/logoipsum4.svg'
import logoipsum5 from '../src/assets/svg/logoipsum5.svg'
import logoipsum6 from '../src/assets/svg/logoipsum6.svg'
import daoDashboard from '../src/assets/svg/icons/dao-dashboard.svg'
import daoTracker from '../src/assets/svg/icons/dao-tracker.svg'
import daoDocs from '../src/assets/svg/icons/dao-docs.svg'
import daoForum from '../src/assets/svg/icons/dao-forum.svg'
import discord from '../src/assets/svg/icons/discord-icon.svg'
import telegram from '../src/assets/svg/icons/telegram-icon.svg'
import api3Docs from '../src/assets/svg/icons/api3-docs.svg'
import api3Github from '../src/assets/svg/icons/api3-github.svg'
import api3Whitepapers from '../src/assets/svg/icons/api3-whitepaper.svg'
import oevLitepaperBlue from '../src/assets/svg/icons/oev-litepaper-blue.svg'
import dapisDifference from '../src/assets/svg/dapis-difference.svg'
import joinAlliance from '../src/assets/svg/join-api3-alliance.svg'

export const data = {
  meta: {
    title: 'API3',
    description: '',
  },
  hero: {
    heading: [
      { left: '', center: '108 ', right: 'data feeds' },
      { left: 'on ', center: '12 ', right: 'blockchains' },
      { left: 'with ', center: '0 ', right: 'bridges' },
      { left: 'and ', center: '0 ', right: 'middlemen' },
    ],
    text: 'API3’s data feeds are aggregated from first-party oracles* directly on the native chain. This means that you always know where your data comes from and never have to rely on cross-chain bridges as a centralized point of failure for your dApp**.',
    button: {
      label: 'Explore data feeds',
      link: '/data-feeds',
    },
    footnotes: [
      '*Oracle nodes operated by the source-level data provider',
      '**In the past two years, bridges have been exploited for over __$2.5B USD__',
    ],
  },

  availability: {
    heading: 'Available on:',
    logos: [
      {
        name: 'Arbitrum',
        image: arbitrum,
      },
      {
        name: 'Avalanche',
        image: avalanche,
      },
      {
        name: 'Polygon',
        image: polygon,
      },
      {
        name: 'Optimism',
        image: optimism,
      },
      {
        name: 'Moonbeam',
        image: moonbeam,
      },
      {
        name: 'Gnosis',
        image: gnosis,
      },
      {
        name: 'Fantom',
        image: fantom,
      },
      {
        name: 'Ethereum',
        image: ethereum,
      },
    ],
  },

  difference: {
    heading: 'How are dAPIs different?',
    color: 'blue',
    text: [
      'API3’s data feeds, dAPIs, get their data directly from the source without relying on third-party middlemen. The dAPI aggregation contract resides on the same chain as the consumer dApp, meaning the data is never bridged over and exposed to bridge exploits. This maximizes the reliability and transparency of our feeds, resulting in safer DeFi for everyone.',
    ],
    image: dapisDifference,
    button: {
      label: 'Go to dAPI Docs',
      link: 'https://docs.api3.org/explore/dapis/what-are-dapis.html',
      isExternal: true,
    },
  },

  services: {
    label: { text: 'For Developers', color: 'emerald' },
    heading:
      'Data services for developers who prioritize security, transparency and accuracy.',
    text: 'API3 builds solutions that bridge the gap between off-chain data and on-chain applications with maximal security and minimal latency. Below you can explore our currently available services.',
    color: 'emerald',
    cards: [
      {
        heading: 'Data feeds',
        text: 'Native-chain aggregated price reference data feeds for more than 100 assets, currently on 12 networks including Polygon (PoS & zkEVM), Arbitrum, Avalance, Optimism and others.',
        color: 'emerald',
        icon: dataFeeds,
        button: { label: 'Learn more', link: '/data-feeds', color: 'emerald' },
      },
      {
        heading: 'QRNG',
        text: 'Free (only pay for gas) quantum mechanics -derived random number generator for smart contracts, powered by Australia National University Quantum Optics Group.',
        color: 'emerald',
        icon: qrng,
        button: { label: 'Learn more', link: '/qrng', color: 'emerald' },
      },
      {
        heading: 'Web3 APIs',
        text: 'A catalog of on-demand APIs made available to Web3 developers through the use of API3’s Airnode oracle technology.',
        color: 'emerald',
        icon: web3Apis,
        button: { label: 'Learn more', link: '/web3-apis', color: 'emerald' },
      },
    ],
  },

  OEV: {
    heading: 'OEV – The antidote to MEV.',
    text: 'Oracle Extractable Value is API3’s solution to minimize the value loss caused by MEV and return extractable value to DeFi dApps and their users, not market makers. OEV is set to launch in 2023 and will be available for all API3 data feed users.',
    color: 'emerald',
    cards: [
      {
        heading: 'Introduction to OEV',
        text: 'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
        color: 'emerald',
        icon: oevIntroduction,
        button: {
          label: 'Take a deep dive into OEV',
          link: 'https://medium.com/api3/oracle-extractable-value-oev-13c1b6d53c5b',
          color: 'emerald',
          isExternal: true,
        },
      },
      {
        heading: 'OEV Litepaper',
        text: 'A more detailed paper on the technical design of API3’s OEV solution.',
        color: 'emerald',
        icon: oevLitepaper,
        button: {
          label: 'Get the details',
          link: 'https://drive.google.com/file/d/1wuSWSI8WY9ChChu2hvRgByJSyQlv_8SO/edit',
          color: 'emerald',
          isExternal: true,
        },
      },
    ],
  },

  partners: {
    heading: 'API3 Partners',
    color: 'blue',
    logos: [
      {
        name: 'A',
        image: logoipsum1,
      },
      {
        name: 'B',
        image: logoipsum2,
      },
      {
        name: 'C',
        image: logoipsum3,
      },
      {
        name: 'D',
        image: logoipsum4,
      },
      {
        name: 'E',
        image: logoipsum5,
      },
      {
        name: 'F',
        image: logoipsum6,
      },
      {
        name: 'A',
        image: logoipsum1,
      },
      {
        name: 'B',
        image: logoipsum2,
      },
      {
        name: 'C',
        image: logoipsum3,
      },
      {
        name: 'D',
        image: logoipsum4,
      },
      {
        name: 'E',
        image: logoipsum5,
      },
      {
        name: 'F',
        image: logoipsum6,
      },
    ],
  },

  applications: {
    label: { text: 'For Data Providers', color: 'blue' },
    heading: 'Connect your data to decentralized applications.',
    color: 'blue',
    cards: [
      {
        text: '__Airnode__ is a simple, crypto-free Web3 API gateway that enables API providers to connect their data to decentralized applications running on various blockchains.',
        color: 'blue',
        icon: airnode,
        button: { label: 'Learn more', link: '/airnode', color: 'blue' },
      },
      {
        text: '__ChainAPI__ is Airnode’s effortless, no-code, GUI-based integration tool that enables you to self-deploy an Airnode for your API in minutes.',
        color: 'blue',
        icon: chainapi,
        button: {
          label: 'Deploy your Airnode with ChainAPI',
          link: '',
          color: 'blue',
        },
      },
    ],
  },

  CTA: {
    heading: 'Join the API3 Alliance.',
    text: 'API3 Alliance is the largest association of API Providers on Web3. It was formed to promote the use of real-world data in decentralized applications.',
    color: 'blue',
    image: joinAlliance,
    button: {
      label: 'View members and join',
      link: 'https://api3dao.typeform.com/to/sM9Ghjew?typeform-source=api3.org',
      color: 'blue',
      isExternal: true,
    },
  },

  DAO: {
    label: { text: 'DAO', color: 'purple' },
    heading: 'API3 is decentrally governed by its token holders.',
    text: 'As a DAO, the API3 project is governed programmatically through the use of an on-chain voting mechanism, powered by the API3 token.',
    color: 'purple',
    cards: [
      {
        heading: 'DAO Dashboard',
        text: 'View and generate proposals, stake API3 and vote on proposals.',
        color: 'purple',
        icon: daoDashboard,
        button: {
          label: 'Learn more',
          link: 'https://api3.eth.limo/',
          color: 'purple',
          isExternal: true,
        },
      },
      {
        heading: 'DAO Tracker',
        text: 'Track DAO metrics like staking rewards, wallet and treasury balances and voting results.',
        color: 'purple',
        icon: daoTracker,
        button: {
          label: 'Learn more',
          link: 'https://tracker.api3.org',
          color: 'purple',
          isExternal: true,
        },
      },
      {
        heading: 'DAO Docs',
        text: 'Learn more about the API3 DAO, its structure and functions within the API3 project.',
        color: 'purple',
        icon: daoDocs,
        button: {
          label: 'Learn more',
          link: 'https://docs.api3.org/explore/dao-members/',
          color: 'purple',
          isExternal: true,
        },
      },
      {
        heading: 'DAO Forum',
        text: 'Discuss proposal topics before they go up to a vote in the DAO.',
        color: 'purple',
        icon: daoForum,
        button: { label: 'Learn more', link: '', color: 'purple' },
      },
    ],
  },

  channels: {
    heading: 'Community channels',
    text: 'Ask questions, get answers, voice your ideas and get involved.',
    color: 'blue',
    platforms: [
      {
        name: 'API3 Discord',
        icon: discord,
        link: 'https://discord.com/invite/qnRrcfnm5W',
        isExternal: true,
      },
      {
        name: 'API3 Telegram',
        icon: telegram,
        link: 'https://t.me/API3DAO',
        isExternal: true,
      },
    ],
  },

  resources: {
    heading: 'Resources',
    color: 'blue',
    cards: [
      {
        heading: 'API3 Docs',
        icon: api3Docs,
        link: 'https://docs.api3.org',
        isExternal: true,
      },
      {
        heading: 'API3 Github',
        icon: api3Github,
        link: 'https://github.com/API3DAO',
        isExternal: true,
      },
      {
        heading: 'API3 Whitepaper',
        icon: api3Whitepapers,
        link: 'https://drive.google.com/file/d/1b8QsGPCJJC1pQOcg83-knD1IAOIgCtZZ/view',
        isExternal: true,
      },
      {
        heading: 'OEV Litepaper',
        icon: oevLitepaperBlue,
        link: 'https://drive.google.com/file/d/1wuSWSI8WY9ChChu2hvRgByJSyQlv_8SO/edit',
        isExternal: true,
      },
    ],
  },
}
