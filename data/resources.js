export const data = {
  meta: {
    title: 'Resources',
    description: 'purple',
  },

  hero: {
    heading: 'Resources',
    color: 'purple',
  },

  docs: {
    heading: 'Explore DOCs',
    text: 'Placeholder text. Random number generators are typically pseudo-random. They can be biased or repeat at some point.',
    color: 'purple',
    button: {
      label: 'Open DOCs',
      link: '',
    },
    cards: [
      'ChainAPI',
      'DAO Members',
      'AIRNODE',
      'QRNG',
      'API3 Overview',
      'dAPIs',
      'OIS',
    ],
  },

  articles: {
    heading: 'API3 articles',
    text: 'Placeholder text. Random number generators are typically pseudo-random. They can be biased or repeat at some point.',
    color: 'blue',
    button: {
      label: 'Read more',
      link: '',
    },
    cards: [
      {
        heading:
          'Placeholder text. Random number generators are typically pseudo-random.',
        readingTime: '3',
        image: '',
      },
      {
        heading:
          'Placeholder text. Random number generators are typically pseudo-random.',
        readingTime: '3',
        image: '',
      },
      {
        heading:
          'Placeholder text. Random number generators are typically pseudo-random.',
        readingTime: '3',
        image: '',
      },
    ],
  },

  whitepapers: {
    heading: 'Whitepapers',
    color: 'blue',
    cards: [
      {
        heading: 'English',
        icon: '',
        link: '',
      },
      {
        heading: 'Chinese | 中文',
        icon: '',
        link: '',
      },
      {
        heading: 'Korean | 한국어',
        icon: '',
        link: '',
      },
      {
        heading: 'Spanish | Español',
        icon: '',
        link: '',
      },
      {
        heading: 'Vietnamese | Tiếng Việt',
        icon: '',
        link: '',
      },
    ],
  },
}
