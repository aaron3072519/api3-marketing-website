import nfts from '../src/assets/svg/icons/nfts.svg'
import gaming from '../src/assets/svg/icons/gaming.svg'
import gambling from '../src/assets/svg/icons/gambling.svg'
import participatoryPocesses from '../src/assets/svg/icons/participatory-processes.svg'
import encrypted from '../src/assets/svg/icons/encrypted.svg'
import neverReused from '../src/assets/svg/icons/never-reused.svg'
import destroyed from '../src/assets/svg/icons/destroyed.svg'
import servers from '../src/assets/svg/icons/aws-servers.svg'
import qrngPreview from '../src/assets/images/qrng.png'
import hero from '../src/assets/images/qrng-hero.svg'
import anu from '../src/assets/images/qrng-anu.svg'

export const data = {
  meta: {
    title: 'QRNG',
    description: '',
  },

  hero: {
    heading: 'Quantum Random Numbers for Smart Contracts. For Free.',
    text: 'API3 and The Australian National University (ANU) Quantum Optics Group bring truly random numbers based on quantum mechanics to smart contracts at no charge. All you pay for is gas.',
    color: 'emerald',
    button: {
      label: 'Get Started Now',
      link: 'https://docs.api3.org/explore/qrng/',
      isExternal: true,
    },
    image: hero,
  },

  ANU: {
    heading: 'Why Quantum Random Numbers?',
    color: 'emerald',
    text: [
      'Random number generators are typically pseudo-random. They can be biased or repeat at some point. This can introduce a security hole that can potentially be exploited.',
    ],
    image: anu,
  },

  mechanics: {
    heading:
      'Quantum mechanics guarantees the numbers we provide are unbiased and maximally secure.',
    color: 'emerald',
    cards: [
      {
        heading: 'NFTs',
        text: 'Randomly distributing assets comes to life in an artistic format when developing generative NFTs – resulting in a collection of dynamic digital assets developed through the organically unpredictable quantum process, compared to the manufactured nature of computer algorithms.',
        icon: nfts,
      },
      {
        heading: 'Gaming',
        text: 'Metaverse, play-to-earn, and community-building games on the blockchain heavily rely on continuous random number generation to keep operations unpredictable and exciting for players.',
        icon: gaming,
      },
      {
        heading: 'Gambling',
        text: 'For games of chance, where financial incentives serve as a reward for correct wagers, players will only partake if they trust developers are utilizing a tamper-proof generation to maintain fair odds.',
        icon: gambling,
      },
      {
        heading: 'Participatory Processes',
        text: 'Web3 applications that involve public participation, such as random token distribution or drawn winners, can leverage a random number generator to ensure a truly fair assignment process of high demand items without bias.',
        icon: participatoryPocesses,
      },
    ],
  },

  hosting: {
    heading: 'Where Is It Hosted?',
    text: 'API3 QRNG is hosted by the ANU Quantum Optics Group in AWS. API3 QRNG numbers are:',
    color: 'emerald',
    cards: [
      {
        heading: 'Encrypted in transmission between ANU, AWS, and Airnode',
        icon: encrypted,
        link: '',
      },
      {
        heading: 'Never reused or served to multiple requests',
        icon: neverReused,
        link: '',
      },
      {
        heading:
          'Destroyed after they are served - we never have access to them',
        icon: destroyed,
        link: '',
      },
      {
        heading: 'Encrypted at rest on AWS servers',
        icon: servers,
        link: '',
      },
    ],
  },

  embed: {
    heading: 'Why is it Free?',
    text: 'Web3 has given so much to API3 and we want to give something back. All that we ask in return is that you acknowledge API3 in your app with a link back to this page. The images below can be included in your page by copy and pasting the HTML.',
    color: 'emerald',
    html: {
      heading: 'HTML emded',
      code: `<a target="_blank" href="https://api3.org/qrng" rel="noopener noreferrer">
      <img width="600px" height="208px" src="https://api3.org/img/quantum-random-numbers/embed-badges/Type=Black, Size=Large.png"></a>`,
      colors: {
        heading: 'Color variant',
        variants: [
          {
            name: 'Black',
            color: '#030303',
          },
          {
            name: 'White',
            color: '#F3F3F3',
          },
          {
            name: 'Green',
            color: '#7CE3CB',
          },
          {
            name: 'Violet',
            color: '#7963B2',
          },
        ],
      },
      sizes: {
        heading: 'Size',
        variants: [
          {
            name: 'Large',
            label: 'L',
            width: '600',
            height: '208',
          },
          {
            name: 'Medium',
            label: 'M',
            width: '256',
            height: '112',
          },
          {
            name: 'Small',
            label: 'S',
            width: '120',
            height: '58',
          },
        ],
      },
      button: {
        label: 'Copy to clipboard ',
      },
    },
    image: qrngPreview,
  },

  CTA: {
    heading: 'Still have questions?',
    color: 'emerald',
    text: 'Contact us on [Discord](https://discord.com/invite/qnRrcfnm5W) or the [API3 Forum](https://forum.api3.org)',
  },
}
