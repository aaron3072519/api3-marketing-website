export const data = {
  meta: {
    title: '404',
    description: '',
  },

  heading: 'Page not found. We can´t seem to find the page you´re looking for.',
  color: 'emerald',
  button: {
    label: 'Go back Home',
    link: '/',
  },
}
