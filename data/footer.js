import logo from '../src/assets/images/api3-logo.png'
import twitter from '../src/assets/svg/icons/twitter-icon-negative.svg'
import discord from '../src/assets/svg/icons/discord-icon-negative.svg'
import telegram from '../src/assets/svg/icons/telegram-icon-negative.svg'
import reddit from '../src/assets/svg/icons/reddit-icon-negative.svg'
import github from '../src/assets/svg/icons/github-icon-negative.svg'
import linkedin from '../src/assets/svg/icons/linkedin-icon-negative.svg'
import youtube from '../src/assets/svg/icons/youtube-icon-negative.svg'

export const data = {
  logo: logo,
  cols: [
    {
      heading: 'For developers',
      items: [
        {
          title: 'Data feeds',
          link: '/data-feeds',
        },
        {
          title: 'QRNG',
          link: '/qrng',
        },
        {
          title: 'Web3 APIs',
          link: '/web3-apis',
        },
      ],
    },
    {
      heading: 'FOR DATA PROVIDERS',
      items: [
        {
          title: 'Airnode',
          link: '/airnode',
        },
        {
          title: 'API3 Alliance',
          link: '/api3-alliance',
        },
      ],
    },
    {
      heading: 'DAO',
      items: [
        {
          title: 'DAO Dashboard',
          link: 'https://api3.eth.limo/',
          isExternal: true,
        },
        {
          title: 'DAO Tracker',
          link: 'https://tracker.api3.org/',
          isExternal: true,
        },
      ],
    },
    {
      heading: 'RESOURCES',
      items: [
        {
          title: 'Docs',
          link: 'https://docs.api3.org',
          isExternal: true,
        },
        {
          title: 'Articles',
          link: 'https://medium.com/api3',
          isExternal: true,
        },
        {
          title: 'Whitepapers',
          link: 'https://drive.google.com/file/d/1b8QsGPCJJC1pQOcg83-knD1IAOIgCtZZ/view',
          isExternal: true,
        },
      ],
    },
    {
      heading: 'PARTICIPATE',
      items: [
        {
          title: 'Github',
          link: 'https://github.com/API3DAO',
          isExternal: true,
        },
        {
          title: 'Open Bank Project',
          link: '/open-bank-project',
        },
        {
          title: 'Open Positions',
          link: '/open-positions',
        },
        {
          title: 'Contact',
          link: '/contact',
        },
      ],
    },
  ],
  row: [
    {
      title: 'Privacy Policy',
      link: '',
    },
    {
      title: 'Privacy and Cookies',
      link: '',
    },
    {
      title: 'Terms & Conditions',
      link: '',
    },
  ],
  socials: [
    { icon: twitter, link: 'https://twitter.com/API3DAO' },
    { icon: discord, link: 'https://discord.com/invite/qnRrcfnm5W' },
    { icon: telegram, link: 'https://t.me/API3DAO' },
    { icon: reddit, link: 'https://www.reddit.com/r/API3/' },
    { icon: github, link: 'https://github.com/API3DAO' },
    { icon: linkedin, link: 'https://www.linkedin.com/company/api3/' },
    { icon: youtube, link: 'https://www.youtube.com/c/API3DAO' },
  ],
}
