export const data = {
  meta: {
    title: 'Contact Us',
    description: '',
  },

  hero: {
    heading: 'Contact Us',
  },

  platforms: {
    color: 'blue',
    cards: [
      {
        name: 'API3 Discord',
        icon: '../src/assets/svg/discord-icon.svg',
        link: '',
      },
      {
        name: 'API3 Telegram',
        icon: '../src/assets/svg/telegram-icon.svg',
        link: '',
      },
      {
        name: 'Contact Support',
        text: 'Brief description of what questions are sent to this mail',
        icon: '',
        email: 'help@api3.org',
      },
      {
        name: 'General Email',
        text: 'Brief description of what questions are sent to this mail',
        icon: '',
        email: 'contact@api3.org',
      },
    ],
  },
}
