export const data = {
  meta: {
    title: 'Web3 APIs',
    description: '',
  },

  hero: {
    heading: 'Build connected smart contracts with world-class APIs.',
    color: 'emerald',
    searchPlaceholder: 'Search for APIs, API Providers',
    button: { label: 'Search APIs', link: '' },
  },

  poweredBy: {
    heading: "Powered by Airnode's request-response protocol.",
    color: 'emerald',
    text: [
      "Airnode is API3's data provider operated Web3 API gateway, which allows smart contracts to call off-chain APIs and receive data from them without any third-party middlemen.",
    ],
    image: '../src/assets/svg/mockImage.svg',
    button: { label: 'Learn more', link: '', color: 'emerald' },
  },

  allTags: [
    'All',
    'Finance',
    'Web3',
    'Weather & Climate',
    'Trading Tools',
    'Price Reference',
    'Open Banking',
    'Lending & Credit Score',
    'AI & Machine Learning',
    'E-sports & Gaming',
    'E-commerce',
    'Credit Scoring & Lending',
    'Blockchain Analytics',
    'Other',
    'Insurance',
    'Marketing',
    'Digital Identity',
  ],

  apis: [
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API1',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: true,
      tags: ['Finance'],
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API2',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: true,
      tags: ['Web3'],
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API3',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: true,
      tags: ['Finance'],
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API4',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: true,
      tags: ['Finance'],
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API5',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: true,
      tags: ['Finance', 'Web3'],
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API6',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: true,
      tags: ['Web3'],
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API7ALLTAGS',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: true,
      tags: [
        'All',
        'Finance',
        'Web3',
        'Weather & Climate',
        'Trading Tools',
        'Price Reference',
        'Open Banking',
        'Lending & Credit Score',
        'AI & Machine Learning',
        'E-sports & Gaming',
        'E-commerce',
        'Credit Scoring & Lending',
        'Blockchain Analytics',
        'Other',
        'Insurance',
        'Marketing',
        'Digital Identity',
      ],
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API8',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API9',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API10',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API11',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API12',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API13',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API14',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API15',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API16',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API17',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
    {
      image: '../src/assets/svg/mockImage.svg',
      heading: 'API18',
      description:
        'Read about what OEV is, how it gets captured and how dApps benefit from integrating API3’s OEV-enabled data feeds.',
      button: { label: 'Learn more', link: '' },
      isFeatured: false,
    },
  ],

  featuredAPIs: {
    heading: 'Featured APIs',
    color: 'emerald',
    paginationArrow: '../src/assets/svg/paginationArrow.svg',
  },

  allAPIs: {
    heading: 'All APIs',
    filter: {
      label: 'Filter of APIs',
      image: '../src/assets/svg/allAPIsFilter.svg',
    },
    button: { label: 'Show more' },
    clearButton: 'Clear',
  },
}
