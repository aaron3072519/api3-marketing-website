export const data = {
  meta: {
    title: 'API3 Alliance',
    description: '',
  },

  hero: {
    heading: 'The Largest Association of API Providers on Web3',
    color: 'blue',
    text: 'Blockchain-based Web3 applications have been limited by their inability to use real world data and services. The API3 Alliance changes that.',
    button: {
      label: 'Join the API3 Alliance',
      link: 'https://api3dao.typeform.com/to/sM9Ghjew?typeform-source=api3.org',
      isExternal: true,
    },
  },

  members: {
    heading: 'Alliance Members',
    text: 'Organizations using Airnode to power their Web3 data services',
    color: 'blue',
    logos: [
      {
        name: 'A',
        image: '../src/assets/svg/logoipsum1.svg',
      },
      {
        name: 'B',
        image: '../src/assets/svg/logoipsum2.svg',
      },
      {
        name: 'C',
        image: '../src/assets/svg/logoipsum3.svg',
      },
      {
        name: 'D',
        image: '../src/assets/svg/logoipsum4.svg',
      },
      {
        name: 'E',
        image: '../src/assets/svg/logoipsum5.svg',
      },
      {
        name: 'F',
        image: '../src/assets/svg/logoipsum6.svg',
      },
      {
        name: 'A',
        image: '../src/assets/svg/logoipsum1.svg',
      },
      {
        name: 'B',
        image: '../src/assets/svg/logoipsum2.svg',
      },
      {
        name: 'C',
        image: '../src/assets/svg/logoipsum3.svg',
      },
      {
        name: 'D',
        image: '../src/assets/svg/logoipsum4.svg',
      },
      {
        name: 'E',
        image: '../src/assets/svg/logoipsum5.svg',
      },
      {
        name: 'F',
        image: '../src/assets/svg/logoipsum6.svg',
      },
    ],
  },

  CTA: {
    heading: 'Join API3 Alliance',
    text: 'If you need a data feed for your dApp but can’t find it on the API3 Market, or are building on a currently unsupported network, please get in touch with us.',
    color: 'blue',
    button: {
      label: 'Apply now',
      link: 'https://api3dao.typeform.com/to/sM9Ghjew?typeform-source=api3.org',
      isExternal: true,
    },
    image: '',
  },

  FAQ: {
    heading: 'FAQ',
    color: 'emerald',
    questions: [
      {
        question: 'Why join the API3 Alliance?',
        answer:
          'The alliance’s mission is to help members successfully expand into Web3. You provide your Web API - we help Web3 developers find and adopt your services. It’s your Web3 go-to-market amplifier.',
      },
      {
        question: 'What does it cost to join?',
        answer:
          'The alliance’s mission is to help members successfully expand into Web3. You provide your Web API - we help Web3 developers find and adopt your services. It’s your Web3 go-to-market amplifier.',
      },
      {
        question: 'Can I charge for my API?',
        answer:
          'The alliance’s mission is to help members successfully expand into Web3. You provide your Web API - we help Web3 developers find and adopt your services. It’s your Web3 go-to-market amplifier.',
      },
    ],
  },
}
