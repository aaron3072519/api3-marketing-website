export const data = {
  meta: {
    title: 'Open Positions',
    description: '',
  },

  hero: {
    heading: 'Core Tech Team Positions',
    color: 'white',
    text: 'Open positions on the Core Tech Team are listed [here](http://) with the technical documentation.',
    button: {
      label: 'See Open Positions',
      link: '',
    },
  },
}
