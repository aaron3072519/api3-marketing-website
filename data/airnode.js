import effortlessOperation from '../src/assets/svg/icons/effortless-operation.svg'
import zeroTrust from '../src/assets/svg/icons/zero-trust.svg'
import multiChain from '../src/assets/svg/icons/multi-chain.svg'
import zeroCost from '../src/assets/svg/icons/zero-cost.svg'
import airnodeDocs from '../src/assets/svg/icons/airnode-docs.svg'
import chainApi from '../src/assets/svg/icons/chain-api.svg'
import chainApiDocs from '../src/assets/svg/icons/chainapi-docs.svg'

export const data = {
  meta: {
    title: 'Airnode',
    description: '',
  },

  hero: {
    heading: 'The Web3 API Gateway',
    color: 'blue',
    text: [
      '__Airnode__ is open source Web3 middleware that connects any web API directly to any blockchain application.',
      '__ChainAPI__ is Airnode’s easy-to-use integration platform that enables any API provider to deploy and manage their Airnode on their own.',
    ],
    buttons: [
      {
        label: 'Connect your API to Web3',
        link: 'https://api3dao.typeform.com/to/ISFd4XPR?typeform-source=api3.org',
        isExternal: true,
      },
      {
        label: 'Go to ChainAPI',
        link: '',
        isSecondary: true,
      },
    ],
  },

  solution: {
    heading: 'Airnode is the Web3 oracle solution for the API economy',
    text: [
      'Open source, serverless and provider-operable – Airnode is the first provider-focused oracle solution for connecting real-world data to blockchain applications.',
    ],
    color: 'blue',
    image: '../src/assets/svg/oracle-solution.svg',
  },

  features: {
    heading: 'Airnode features & benefits',
    color: 'blue',
    features: [
      {
        heading: 'Effortless operation',
        text: 'Airnode is set-and-forget. Built entirely on high-availability serverless infrastructure, once deployed, Airnode requires zero maintenance.',
        icon: effortlessOperation,
      },
      {
        heading: 'Zero trust',
        text: 'Airnode connects your API to the blockchain directly, without any external dependencies in the form of third party oracle node operators.',
        icon: zeroTrust,
      },
      {
        heading: 'Multi-Chain',
        text: 'Airnode is compatible with most EVM-based blockchains, including Ethereum, Polygon PoS and zkEVM, Arbitrum, Avalanche, BNB Chain, Fantom and many more.',
        icon: multiChain,
      },
      {
        heading: 'Zero cost',
        text: 'Airnode is open source and free to use under the MIT Licence. It also does not require you to handle cryptocurrencies, as the requester pays for the blockchain gas fees.',
        icon: zeroCost,
      },
    ],
  },

  CTA: {
    heading:
      'The future of the internet is happening now. It’s called Web3. Airnode is how you connect.',
    text: 'Go to ChainAPI to deploy your Airnode.',
    color: 'blue',
    button: {
      label: 'Go to ChainAPI',
      link: '',
    },
  },

  resources: {
    heading: 'Resources',
    color: 'blue',
    cards: [
      {
        heading: 'Airnode Docs',
        icon: airnodeDocs,
        link: 'https://docs.api3.org/explore/airnode/what-is-airnode.html',
        isExternal: true,
      },
      {
        heading: 'ChainAPI',
        icon: chainApi,
        link: '',
      },
      {
        heading: 'ChainAPI Docs',
        icon: chainApiDocs,
        link: '',
      },
    ],
  },
}
