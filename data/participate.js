import github from '../src/assets/svg/icons/github.svg'
import openBankProject from '../src/assets/svg/icons/open_bank_project.svg'
import university from '../src/assets/svg/icons/university.svg'
import openPositions from '../src/assets/svg/icons/open_positions.svg'

export const data = {
  meta: {
    title: 'Participate',
    description: '',
  },

  hero: {
    heading: 'API3 Alliance',
    color: 'white',
    text: 'Lorem ipsum dolor sit amet consectetur. Commodo diam tellus tristique et viverra eu lectus. Amet bibendum sagittis a nisl elit ullamcorper.',
    button: {
      label: 'Learn More',
      link: '',
    },
  },

  cards: [
    {
      heading: 'Github',
      text: 'Lorem ipsum dolor sit amet consectetur.',
      color: 'emerald',
      image: github,
      button: {
        label: 'Learn More',
        link: '',
        color: 'green',
      },
    },
    {
      heading: 'Open Bank Project',
      text: 'Lorem ipsum dolor sit amet consectetur.',
      color: 'emerald',
      image: openBankProject,
      button: {
        label: 'Learn More',
        link: '',
        color: 'green',
      },
    },
    {
      heading: 'University',
      text: 'Lorem ipsum dolor sit amet consectetur.',
      color: 'blue',
      image: university,
      button: {
        label: 'Learn More',
        link: '',
        color: 'blue',
      },
    },
    {
      heading: 'Open Positions',
      text: 'Lorem ipsum dolor sit amet consectetur.',
      color: 'blue',
      image: openPositions,
      button: {
        label: 'Learn More',
        link: '',
        color: 'blue',
      },
    },
  ],

  CTA: {
    heading: 'Contact',
    text: 'Lorem ipsum dolor sit amet consectetur.',
    color: 'blue',
    image: '',
    button: {
      label: 'Learn More',
      link: '',
    },
  },
}
