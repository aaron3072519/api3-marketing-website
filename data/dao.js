export const data = {
  meta: {
    title: 'DAO',
    description: '',
  },

  hero: {
    heading: 'Decentralized Autonomous Organization',
    text: 'Placeholder text. API3 and The Australian National University (ANU) Quantum Optics Group bring truly random.',
    color: 'emerald',
    button: {
      label: 'Launch the DAO Dashboard',
      link: '',
    },
    image: '',
  },

  cards: [
    {
      heading: 'DAO Tracker',
      text: [
        'Placeholder text. Random number generators are typically pseudo-random. They can be biased or repeat at some point. This can introduce a security hole that can potentially be exploited.',
      ],
      color: 'blue',
      image: '',
    },
    {
      heading: 'The Governance Token of the API3 DAO',
      text: [
        'API3 is the native token of the API3 project. It gives its holders the right to take part in the governance of the API3 ecosystem through the API3 DAO.',
      ],
      color: 'emerald',
      image: '',
      button: {
        label: 'Read more',
        link: '',
      },
    },
  ],

  coverage: {
    heading: 'Coverage staking pool',
    text: 'Coverage staking pool funds are used to cover potential financial losses from dAPI malfunctions that the dAPI consumer might incur.',
    color: 'emerald',
    cards: [
      {
        heading: 'Coverage Pool (API3)',
        number: '61 985 725',
      },
      {
        heading: 'Annual Rewards (APY)',
        number: '29.29%',
      },
    ],
    button: {
      label: 'Go to App',
      link: '',
    },
  },

  socials: {
    heading: 'DAO Socials',
    text: 'Placeholder text. As a DAO, the API3 project is governed programmatically through the use of an on-chain voting mechanism, powered by the API3 token.',
    color: 'blue',
    platforms: [
      {
        name: 'DAO Forum',
        text: 'View and generate proposals, stake API3 and vote on proposals. ',
        icon: '',
        link: '',
      },
      {
        name: 'DAO Discord',
        text: 'View and generate proposals, stake API3 and vote on proposals. ',
        icon: '../src/assets/svg/discord.svg',
        link: '',
      },
      {
        name: 'DAO Telegram',
        text: 'View and generate proposals, stake API3 and vote on proposals. ',
        icon: '../src/assets/svg/telegram.svg',
        link: '',
      },
    ],
  },

  CTA: {
    heading: 'CTA Section',
    text: 'Data source transparent. Quantifiably secure.',
    color: 'blue',
    button: {
      label: 'Get Started Now',
      link: '',
    },
  },
}
