export const data = {
  meta: {
    title: 'Data Feeds',
    description: '',
  },

  hero: {
    heading: 'dAPIs',
    text: 'Power your dApp with the most secure data feeds in DeFi.',
    color: 'emerald',
    button: {
      label: 'Go to Market',
      link: 'https://market.api3.org/dapis',
      isExternal: true,
    },
    image: '',
  },

  features: {
    heading: 'dAPI Features',
    text: 'API3 price feeds (dAPIs) source their data directly from high-quality data providers, without security-reducing middlemen. dAPI data updates are always aggregated on the native chain, meaning your dApp will not be vulnerable to bridge exploits.',
    image: '',
    color: 'emerald',
    features: [
      {
        heading: 'Native-chain aggregated',
        text: [
          'Cross-chain bridges have historically posed an at best expensive and at worst existential risk to dApps that rely on them as a centralized point of failure.',
          'API3’s dAPIs aggregate their data directly on the native chain, improving the security of your dApp, and eliminating the risk of getting caught in the next bridge hack.',
        ],
        button: {
          label: 'Learn more',
          link: '',
        },
        image: '',
      },
      {
        heading: 'First-party',
        text: 'Thanks to our provider-operated oracle node, Airnode, API3 data feeds are powered by data directly from some of the world’s most premier asset price data providers. Since dAPIs aggregate data without middleman node operators, the data sources used by dAPIs are always transparent.',
        button: {
          label: 'Become a Data Provider',
          link: '',
        },
        image: '',
      },
      {
        heading: 'Multi-platform',
        text: 'You can utilize dAPIs for your dApp on all of the most popular EVM-compatible networks, such as Ethereum, Polygon, Arbitrum, Avalanche, Optimism, BNB Chain and many others. We strive to support the development of a multi-chain future, and will add support for new networks as they gain popularity.',
        button: {
          label: 'Request a Network',
          link: 'https://api3dao.typeform.com/to/O1Uvxc8m?typeform-source=api3.org',
          isExternal: true,
        },
        image: '',
      },
      {
        heading: 'Simple to Integrate',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed mi sed leo ultricies iaculis. Suspendisse nec nisi eros. Suspendisse potenti. Nulla bibendum molestie purus et pharetra. Proin eget porttitor ipsum, a volutpat lorem. Morbi accumsan, nibh nec egestas finibus, dui ex porttitor eros, et pulvinar diam metus vitae orci.',
        button: {
          label: 'Browse dAPIs',
          link: '',
        },
        image: '',
      },
    ],
  },

  integration: {
    heading: 'Integrate a dAPI',
    color: 'purple',
    button: {
      label: 'Visit dAPI Docs to get started',
      link: 'https://docs.api3.org/explore/dapis/what-are-dapis.html',
      isExternal: true,
    },
    video: '',
  },

  oracle: {
    heading: 'Oracle Extractable Value',
    text: '__Get infinitely granular*__ oracle updates and recapture value normally lost to MEV back to your dApp and its users instead of block producers and market makers.',
    color: 'purple',
    note: 'Oracle Extractable Value by API3 coming soon.',
    buttons: [
      {
        label: 'Litepaper',
        link: 'https://drive.google.com/file/d/1wuSWSI8WY9ChChu2hvRgByJSyQlv_8SO/edit',
        isExternal: true,
      },
      {
        label: 'Introduction to OEV',
        link: 'https://medium.com/api3/oracle-extractable-value-oev-13c1b6d53c5b',
        isSecondary: true,
      },
    ],
    footnotes: [
      '*OEV data feed updates do not rely on price deviation thresholds and are not limited by gas prices, making them as high-fidelity as the underlying data sources.',
    ],
  },

  CTA: {
    heading: 'Can’t find what you need?',
    text: 'If you need a data feed for your dApp but can’t find it on the API3 Market, or are building on a currently unsupported network, please get in touch with us.',
    color: 'blue',
    button: {
      label: 'Request a data feed',
      link: 'https://api3dao.typeform.com/to/O1Uvxc8m?typeform-source=api3.org',
      isExternal: true,
    },
  },

  resources: {
    heading: 'Data Feed Resources',
    color: 'blue',
    cards: [
      {
        heading: 'API3 Market',
        icon: '',
        link: 'https://market.api3.org/dapis',
        isExternal: true,
      },
      {
        heading: 'dAPI Integration Guide',
        icon: '',
        link: 'https://docs.api3.org/explore/dapis/using-dapis.html',
        isExternal: true,
      },
      {
        heading: 'API3 Docs',
        icon: '',
        link: 'https://docs.api3.org',
        isExternal: true,
      },
      {
        heading: 'API3 GitHub',
        icon: '',
        link: 'https://github.com/API3DAO',
        isExternal: true,
      },
    ],
  },
}
