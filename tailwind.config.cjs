/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    './src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}',
    './utils/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"',
  ],
  theme: {
    extend: {
      gridTemplateColumns: {
        '8outliner': 'repeat(8, min-content)',
        '6outliner': 'repeat(6, min-content)',
        '1outliner': 'repeat(1, auto-fit)',
      },
      gridTemplateRows: {
        '8outliner': 'repeat(2, min-content)',
      },
      backgroundImage: {
        homepage: "url('./src/assets/svg/homepageBG.svg')",
        web3APIs: "url('./src/assets/svg/web3APIsBG.svg')",
        search: "url('./src/assets/svg/searchbarSearch.svg')",
        cross: "url('./src/assets/svg/searchbarCross.svg')",
        arrow: "url('./src/assets/svg/checkmark.svg')",
        
        link_white_arrow: "url('./src/assets/svg/linkWhiteArrow.svg')",
        link_light_white_arrow: "url('./src/assets/svg/linkWhiteArrow.svg')",

        link_emerald_arrow: "url('./src/assets/svg/linkEmeraldArrow.svg')",
        link_light_emerald_arrow: "url('./src/assets/svg/linkLightEmeraldArrow.svg')",

        link_blue_arrow: "url('./src/assets/svg/linkBlueArrow.svg')",
        link_light_blue_arrow: "url('./src/assets/svg/linkLightBlueArrow.svg')",

        link_purple_arrow: "url('./src/assets/svg/linkPurpleArrow.svg')",
        link_light_purple_arrow: "url('./src/assets/svg/linkLightPurpleArrow.svg')",
      },
      colors: {
        primary: {
          white: '#F3F3F3',
          black: '#030303',
          emerald: '#7CE3CB',
          violet: '#7963B2',
        },
        masterBlue: {
          100: '#5378FF',
          500: '#194AFF',
          900: '#0735E1',
        },
        secondary: {
          black: '#818181',
          gray: '#FCFCFF',
          blue30: '#718FFF',
          blue40: '#5378FF',
        },
        tertiary: {
          black: '#E6E6E6',
          black3: '#9A9A9A',
          black5: '#CDCDCD',
          blue: '#C1DEFF',
          emerald: '#DDFFF9',
          purple: '#D5CBF4',
        },
        'gray-rgba': 'rgba(14, 23, 54, 0.3)',
        'white-rgba': 'rgba(255, 255, 255, 0.05)',
        'green-rgba': 'rgba(15, 43, 41, 0.5)',
        'blue-rgba': 'rgba(7, 16, 48, 0.5)',
        'violet-rgba': 'rgba(54, 24, 70, 0.5)',
        'light-gray-rgba': 'rgba(243, 243, 243, 0.11)',
        'light-green-rgba': 'rgba(124, 227, 203, 0.8)',
      },
      fontSize: {
        '1xl': ['1.375rem', '1.5'],
        '3.5xl': ['2rem', '1.3'],
        '4.5xl': ['2.5rem', '1.1'],
        '5.5xl': ['3.5rem', '1.1'],
        '7xl': ['4rem', '1.1'],
        14: '56px',
      },
      spacing: {
        2.5: '10px',
        4.5: '18px',
        10.5: '42px',
        13: '52px',
        15: '62px',
        18: '72px',
        19: '73px',
        25: '100px',
        30: '120px',
        34: '140px',
        38.5: '150px',
        46: '180px',
        48: '200px',
        50: '204px',
        63: '254px',
        68: '274px',
        70: '286px',
        75: '300px',
      },
      maxWidth: {
        200: '50rem',
        '3.5xl': '52rem',
      },
      lineHeight: {
        5.4: '19.6px',
        5.5: '21px',
        5.6: '22.4px',
        15.1: '61.6px',
      },
      boxShadow: {
        card: 'inset 0px 4px 60px rgba(87, 12, 99, 0.1)',
      },
      screens: {
        sm: '345px',
        md: '724px',
        lg: '1088px',
        xl: '1280px',
        '2xl': '1536px',
      },
      container: {
        screens: {
          sm: '345px',
          md: '680px',
          lg: '1088px',
        },
      },
    },
  },
  plugins: [],
}
