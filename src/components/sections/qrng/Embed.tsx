import classNames from 'classnames'
import { useState, useRef } from 'react'
import {
  textColor,
  backgroundGradient,
  sectionMarginBottom,
} from '../../../../utils/classNames'

import { MarkdownText } from '../../MarkdownText'

import copyIcon from '../../../assets/svg/copy-icon.svg'

export interface Props {
  heading: string
  text: string
  color: string
  html: {
    heading: string
    code: string
    colors: {
      heading: string
      variants: { name: string; color: string }[]
    }
    sizes: {
      heading: string
      variants: { name: string; label: string; width: string; height: string }[]
    }
    button: { label: string }
  }
  image: string
}

const ColorVariant = ({ name, color, activeColor, onClick }) => (
  <div
    className={classNames(
      'flex h-12 w-full cursor-pointer items-center justify-center rounded-lg border-2 p-[3px] md:w-12',
      activeColor === name
        ? 'border-light-green-rgba'
        : 'border-light-gray-rgba'
    )}
    onClick={onClick}
  >
    <div
      className={classNames('h-[38px] w-full rounded-[4px] md:w-[38px]', color)}
    ></div>
  </div>
)

const SizeVariant = ({ name, label, activeSize, onClick }) => (
  <div
    className={classNames(
      'mr-4 flex h-12 w-full cursor-pointer items-center justify-center rounded-lg border-2 text-lg font-medium text-white text-opacity-[0.76] md:w-12',
      activeSize === name
        ? 'border-light-green-rgba bg-[#64B6A3]'
        : 'border-light-gray-rgba bg-[#091921]'
    )}
    onClick={onClick}
  >
    {label}
  </div>
)

const Embed = ({ heading, text, color, html, image }: Props) => {
  const colors = {
    Black: 'bg-[#030303]',
    White: 'bg-[#F3F3F3]',
    Green: 'bg-[#7CE3CB]',
    Violet: 'bg-[#7963B2]',
  }
  const headingColor = color ? `text-gradient-${color}` : 'text-primary-white'

  const [activeColor, setActiveColor] = useState('Black')
  const [activeSize, setActiveSize] = useState('Large')
  const [pixels, setPixels] = useState({ width: '600', height: '208' })
  const [embedHTML, setEmbedHTML] = useState(html.code)
  const [buttonLabel, setButtonLabel] = useState(html.button.label)
  const [previewImage, setPreviewImage] = useState(image)
  const ref = useRef(null)

  const handleAttributesChange = (
    color: string,
    size: string,
    pixels: { width: string; height: string }
  ) => {
    const newPixels = pixels
    const newColor = color

    setActiveColor(newColor)
    setActiveSize(size)
    setPixels(newPixels)
    setEmbedHTML(`<a target="_blank" href="https://api3.org/qrng" rel="noopener noreferrer">
    <img width="${pixels.width}px" height="${pixels.height}px" src="https://api3.org/img/quantum-random-numbers/embed-badges/Type=${newColor}, Size=${size}.png"></a>`)
    setPreviewImage(
      `https://api3.org/img/quantum-random-numbers/embed-badges/Type=${newColor}, Size=${size}.png`
    )
  }

  const copyHTML = () => {
    const embedHTML = ref.current.innerText
    navigator.clipboard.writeText(embedHTML)
    setButtonLabel('Copied')
  }

  return (
    <section className={sectionMarginBottom}>
      <div className="flex flex-col-reverse gap-16 lg:flex-row">
        <div className="mb-[94px] border-b border-solid border-white border-opacity-20">
          <div
            className={classNames(
              'mb-4 text-center text-3.5xl font-bold lg:text-left',
              headingColor
            )}
          >
            {html.heading}
          </div>
          <div
            className={classNames(
              'mb-6 rounded-2xl border border-solid px-6 pb-12 pt-8 text-sm text-primary-white lg:mb-10',
              backgroundGradient[color]
            )}
            ref={ref}
          >
            <div className="max-w-[416px] md:mx-auto">{embedHTML}</div>
          </div>
          <div className="flex flex-col md:flex-row md:items-start md:justify-between">
            <div>
              <div className="mb-4 text-lg text-tertiary-black md:text-xl">
                {html.colors.heading}
              </div>
              <div className="mb-8 flex items-center justify-between gap-0 md:mb-14 md:gap-4 lg:mb-10">
                {html.colors.variants.map(
                  (color: { name: string; color: string }) => (
                    <ColorVariant
                      name={color.name}
                      color={colors[color.name]}
                      activeColor={activeColor}
                      onClick={() =>
                        handleAttributesChange(color.name, activeSize, pixels)
                      }
                      key={color.name}
                    />
                  )
                )}
              </div>
            </div>
            <div>
              <div className="mb-4 flex items-center justify-start md:justify-between">
                <div className="mr-5 text-lg text-tertiary-black md:mr-0 md:text-xl">
                  {html.sizes.heading}
                </div>
                <div className="text-sm text-tertiary-black">
                  {pixels.width}x{pixels.height}px
                </div>
              </div>
              <div className="mb-[50px] flex items-center justify-between md:mb-0">
                {html.sizes.variants.map(
                  (size: {
                    name: string
                    label: string
                    width: string
                    height: string
                  }) => (
                    <SizeVariant
                      name={size.name}
                      label={size.label}
                      activeSize={activeSize}
                      onClick={() =>
                        handleAttributesChange(activeColor, size.name, {
                          width: size.width,
                          height: size.height,
                        })
                      }
                      key={size.name}
                    />
                  )
                )}
              </div>
            </div>
          </div>
          <div
            className="mb-[49px] flex cursor-pointer justify-center font-medium text-primary-emerald md:mb-[54px] md:justify-start lg:mb-10.5"
            onClick={copyHTML}
          >
            <img className="mr-[10px]" src={copyIcon} />
            <div>{buttonLabel}</div>
          </div>
        </div>
        <div className="mx-auto mb-16 mt-0 max-w-lg lg:mb-0 lg:mt-[58px]">
          <div
            className={classNames(
              'mb-4 text-center text-4xl font-semibold md:text-5xl lg:text-left lg:text-5.5xl',
              headingColor
            )}
          >
            {heading}
          </div>
          <div
            className={classNames(
              'text-center text-base md:text-xl lg:text-left',
              textColor[color]
            )}
          >
            <MarkdownText text={text} />
          </div>
        </div>
      </div>
      <div className="w-full lg:max-w-lg">
        <img
          src={previewImage}
          alt="API3 QRNG"
          width={pixels.width}
          height={pixels.height}
        />
      </div>
    </section>
  )
}

export default Embed
