import React, { useEffect, useState } from 'react'

export type LabeledCustomCheckboxProps = {
  checkboxValue: string
  activeFilters: string[]
  setActiveFilters: (arr: string[]) => void
  checked: boolean
  firstTag: string
  allTags: string[]
}

function LabeledCustomCheckbox(props: LabeledCustomCheckboxProps) {
  const {
    checkboxValue,
    activeFilters,
    setActiveFilters,
    checked,
    firstTag,
    allTags,
  } = props
  const isFilterActive = activeFilters.includes(checkboxValue)

  const [isChecked, setIsChecked] = useState<boolean>(isFilterActive)

  useEffect(() => {
    setIsChecked(checked)
  }, [activeFilters])

  function onCheckHandler(e: React.ChangeEvent<HTMLInputElement>) {
    const currentFilter = e.target.value

    if (e.target.checked) {
      const newFilters = isFilterActive
        ? activeFilters
        : checkboxValue === firstTag
        ? allTags
        : activeFilters.concat(currentFilter)

      setActiveFilters(newFilters)
    } else {
      const nf = activeFilters.filter((f) => f !== currentFilter)

      const newFilters = isFilterActive ? nf : activeFilters

      setActiveFilters(newFilters)
    }
  }

  return (
    <div className="flex cursor-pointer flex-row items-center gap-[10px]">
      <label className="relative block">
        <input
          className={
            `
            h-[18px] w-[18px]
            appearance-none rounded-[3px]
            border-[1.5px] bg-transparent
            bg-center bg-no-repeat
          ` +
            (isChecked
              ? ' border-primary-emerald bg-arrow'
              : ' border-primary-white')
          }
          type="checkbox"
          onChange={(e) => {
            setIsChecked(!isChecked)
            onCheckHandler(e)
          }}
          value={checkboxValue}
          checked={isChecked}
        />
      </label>
      <div className="text-sm leading-5.4">{checkboxValue}</div>
    </div>
  )
}

const memorizedLabeledCustomCheckbox = React.memo(LabeledCustomCheckbox)

export default memorizedLabeledCustomCheckbox
