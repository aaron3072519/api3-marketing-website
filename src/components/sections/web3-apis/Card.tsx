import Link from '../../elements/Link'

type CardProps = {
  api: {
    image: string
    heading: string
    description: string
    button: { label: string; link: string }
  }
  imgHeight?: string
}

export default function Card(props: CardProps) {
  const { api, imgHeight } = props

  return (
    <>
      <img
        src={api.image}
        alt={api.heading}
        className={
          'h-[150px] w-full rounded-lg object-cover md:h-[150px] lg:h-[176px] ' +
          (imgHeight ? imgHeight : '')
        }
      />
      <p
        className="
          mt-[18px] h-[30px] 
          w-full text-ellipsis 
          text-start text-xl font-bold
          leading-[30px] md:text-[22px] md:leading-[28.6px]
          lg:h-[42px] lg:text-[32px] lg:leading-[41.6px]
        "
      >
        {api.heading}
      </p>
      <p className="mt-[12px] mb-6 h-[110px] w-full text-start text-[16px] leading-5.6 text-[#E6E6E6]">
        {api.description}
      </p>
      <div className="text-left">
        <Link {...api.button} />
      </div>
    </>
  )
}
