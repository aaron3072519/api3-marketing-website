import React, { useEffect, useState, useRef } from 'react'
import L from 'lodash'
import { useMediaQuery } from '../../hooks/useMediaQuery'

import Card from './Card'

type FeaturedAPIsProps = {
  heading: string
  paginationArrow: string
  featured: {
    image: string
    heading: string
    description: string
    button: { label: string; link: string }
    isFeatured: boolean
  }[]
  header?: React.ReactNode
}

export default function FeaturedAPIs(props: FeaturedAPIsProps) {
  const { header, paginationArrow, featured } = props

  const [sliderScroll, setSliderScroll] = useState<number>(0)
  const [maxScroll, setMaxScroll] = useState<number>()

  const isMobile = useMediaQuery('(max-width: 723px)')
  const featuredAPIs = featured.filter((api) => api.isFeatured)

  const isMax = sliderScroll === maxScroll
  const isMin = sliderScroll === 0

  const scrollBy = 200

  const gridContainerRef = useRef<HTMLDivElement>(null)

  const handleSliderScroll = (newScroll: number) => {
    if (gridContainerRef.current) {
      gridContainerRef.current.scrollLeft = newScroll
    }
    setSliderScroll(newScroll)
  }

  const handleScroll = L.debounce(() => {
    if (gridContainerRef.current) {
      setSliderScroll(gridContainerRef.current.scrollLeft)
    }
  }, 10)

  useEffect(() => {
    const gridContainer = gridContainerRef.current
    if (gridContainer) {
      const maxScroll = gridContainer.scrollWidth - gridContainer.clientWidth
      setMaxScroll(maxScroll)

      gridContainer.addEventListener('scroll', handleScroll)
    }

    return () => {
      if (gridContainer) {
        gridContainer.removeEventListener('scroll', handleScroll)
      }
    }
  }, [])

  const scrollRight = () => {
    handleSliderScroll(
      sliderScroll + scrollBy > maxScroll ? maxScroll : sliderScroll + scrollBy
    )
  }

  const scrollLeft = () => {
    handleSliderScroll(
      sliderScroll - scrollBy <= 0 ? 0 : sliderScroll - scrollBy
    )
  }

  return (
    <div className="flex max-w-[345px] flex-col self-center text-start md:max-w-[680px] md:text-center lg:max-w-[1088px]">
      <div className="mb-3 w-fit md:mb-16 lg:mb-20">{props.header}</div>
      <div
        className="
          no-scrollbar border-box
          grid grid-flow-col
          gap-[19.5px]
          overflow-x-scroll text-primary-white lg:gap-6
        "
        ref={gridContainerRef}
      >
        {featuredAPIs.map((api, i) => (
          <React.Fragment key={'FEATURED_APIS' + api.heading + i}>
            <div
              className="
                flex h-[366px] w-[200px]
                flex-col overflow-hidden md:h-[364px]
                md:w-[200px] lg:h-[404px] lg:w-[235.25px]
              "
            >
              <Card api={api} />
            </div>
            {i + (1 % 2) !== 0 && i + 1 !== featuredAPIs.length ? (
              <div className="box-border h-full w-0 border-r-[1px] border-[#D8F7EF33]" />
            ) : (
              <></>
            )}
          </React.Fragment>
        ))}
      </div>
      <div className="mt-4 flex h-8 select-none flex-row justify-between text-white md:mt-20">
        {!isMobile && (
          <img
            className={
              'rotate-180 ' +
              (isMin ? 'cursor-default opacity-20' : 'cursor-pointer')
            }
            src={paginationArrow}
            alt=""
            onClick={isMin ? undefined : scrollLeft}
          />
        )}

        <div className="mt-4 flex h-full w-full flex-row items-center md:mt-0 md:w-[286px]">
          <input
            onChange={(e) => handleSliderScroll(e.target.valueAsNumber)}
            value={sliderScroll}
            className="
              box-border h-[2px] 
              w-full appearance-none
              rounded-full bg-[#d8f7ef33] 
            "
            type="range"
            max={maxScroll}
            min={0}
          />
        </div>

        {!isMobile && (
          <img
            className={isMax ? 'cursor-default opacity-20' : 'cursor-pointer'}
            src={paginationArrow}
            alt=""
            onClick={isMax ? undefined : scrollRight}
          />
        )}
      </div>
    </div>
  )
}
