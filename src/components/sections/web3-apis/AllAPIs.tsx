import React, { useState } from 'react'
import L from 'lodash'
import { useMediaQuery } from '../../hooks/useMediaQuery'

import APIsFilter from './APIsFilter'
import Card from './Card'

type AllAPIsProps = {
  heading: string
  filter: {
    label: string
    image: string
  }
  apis: {
    image: string
    heading: string
    description: string
    button: { label: string; link: string }
    isFeatured?: boolean
    tags?: string[]
  }[]
  allTags: string[]
  clearButton: string
  showMoreButton?: React.ReactNode
}

function AllAPIs(props: AllAPIsProps) {
  const { heading, filter, apis, allTags, clearButton, showMoreButton } = props
  const [activeFilters, setActiveFilters] = useState<string[]>(allTags)
  const [currAPIsPage, setCurrAPIsPage] = useState<number>(2)

  const firstTag = allTags[0]
  const isMobile = useMediaQuery('(max-width: 723px)')
  const isTablet = useMediaQuery('(max-width: 1087px)')

  const activeFiltersButFirst = activeFilters.filter((f) => f !== firstTag)
  const isAllTagsSelected = L.difference(allTags, activeFilters).length === 0
  const allAPIsFiltered =
    activeFiltersButFirst.length === 0 || isAllTagsSelected
      ? apis
      : apis.filter(
          (api) => L.difference(activeFiltersButFirst, api.tags).length === 0
        )

  const cols = isMobile ? 1 : isTablet ? 3 : 4

  const allAPIsChunked = L.chunk(allAPIsFiltered, cols)
  const currAPIs = L.take(allAPIsChunked, currAPIsPage)

  return (
    <div className="flex h-auto max-w-[345px] flex-col pt-30 text-white md:max-w-[680px] md:pt-[200px] lg:max-w-none lg:pt-75 ">
      <div className="flex flex-row justify-between overflow-hidden ">
        <p
          className="
            text-gradient-green w-fit min-w-max
            text-[36px] font-semibold leading-[46.8px]
            tracking-[-1.5px] text-primary-white
            md:text-[48px] md:leading-[52.8px] lg:text-14
            lg:leading-15.1
          "
        >
          {heading}
        </p>
        <APIsFilter
          allTags={allTags}
          clearButton={clearButton}
          activeFilters={activeFilters}
          setActiveFilters={setActiveFilters}
          firstTag={firstTag}
          filter={filter}
        />
      </div>

      <div className="grid grid-rows-8outliner gap-12 pt-12 md:gap-[25.5px] md:pt-16 lg:gap-8 lg:pt-20">
        {currAPIs.map((page, i) => (
          <React.Fragment key={'allAPIsRows' + i}>
            <div
              className="
                grid grid-cols-1outliner text-primary-white md:grid-cols-6outliner
                lg:grid-cols-8outliner
              "
            >
              {page.map((api, i_) => (
                <React.Fragment key={'allAPIsCols' + i_}>
                  <div
                    className={`

                        flex h-[430px] w-full
                        flex-col overflow-hidden md:h-[361px]
                        md:w-[200px] lg:h-[404px] lg:w-[235.25px]
                      `}
                  >
                    <Card api={api} imgHeight={isMobile ? 'h-[258px]' : ''} />
                  </div>
                  {(i_ + 1) % cols !== 0 &&
                  i_ + 1 !== page.length &&
                  cols !== 1 ? (
                    <div
                      className={
                        'h-[430px] w-0 border-r border-[#D8F7EF33] md:mx-[19.5px] md:h-[361px] lg:mx-6 lg:h-[404px]' +
                        (isMobile ? ' mt-6' : '')
                      }
                    />
                  ) : (
                    <></>
                  )}
                </React.Fragment>
              ))}
            </div>
            {i + 1 !== currAPIs.length ||
            (cols === 1 && i + 1 !== currAPIs.length) ? (
              <div className="box-border h-0 w-full border-t border-[#D8F7EF33] " />
            ) : (
              <></>
            )}
          </React.Fragment>
        ))}
      </div>

      {currAPIsPage + 1 <= allAPIsChunked.length ? (
        <div
          className="mt-16 mb-40 flex justify-center text-center"
          onClick={() =>
            currAPIsPage <= allAPIsChunked.length
              ? setCurrAPIsPage(currAPIsPage + 1)
              : {}
          }
        >
          {showMoreButton}
        </div>
      ) : (
        <div className="mb-40 md:mb-[200px] lg:mb-75"></div>
      )}
    </div>
  )
}

export default AllAPIs
