import React, { useRef, useState } from 'react'
import LabeledCustomCheckbox from './LabeledCheckbox'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'

type FilterButtonProps = {
  filter: { label: string; image: string }
  isFilterOpen: boolean
  setIsFilterOpen: (isOpen: boolean) => void
}

export function FilterButton(props: FilterButtonProps) {
  const { filter, isFilterOpen, setIsFilterOpen } = props

  return (
    <div
      className="flex cursor-pointer flex-row items-center"
      onClick={() => setIsFilterOpen(!isFilterOpen)}
    >
      <img src={filter.image} alt="" />
      <p className="ml-2 text-base font-medium leading-5.6 text-primary-emerald">
        {filter.label}
      </p>
    </div>
  )
}

type FilterDropDownProps = {
  apisFilterButtonProps: FilterButtonProps
  allTags: string[]
  clearButton: string
  activeFilters: string[]
  setActiveFilters: (filters: string[]) => void
  firstTag: string
  setIsFilterOpen: (isOpen: boolean) => void
}

function FilterDropDown(props: FilterDropDownProps) {
  const ref = useRef()
  useOnClickOutside(ref, () => setIsFilterOpen(false))

  const {
    apisFilterButtonProps,
    allTags,
    clearButton,
    activeFilters,
    setActiveFilters,
    firstTag,
    setIsFilterOpen,
  } = props

  return (
    <div className="flex flex-col items-center" ref={ref}>
      <div className="h-full w-[241px]" />
      <div
        id="filterDropDown"
        className="
          absolute z-10 
          mt-[31px] w-[241px] 
          rounded-lg bg-[#181D2C] 
          px-6 pt-[25px]
        "
      >
        <FilterButton {...apisFilterButtonProps} />
        <div className="mt-[33px] flex flex-col gap-6">
          {allTags.map((tag, i) => (
            <LabeledCustomCheckbox
              checkboxValue={tag}
              activeFilters={activeFilters}
              setActiveFilters={setActiveFilters}
              checked={activeFilters.includes(tag)}
              firstTag={firstTag}
              key={tag + i}
              allTags={allTags}
            />
          ))}
        </div>
        <div className="mt-4 border-b border-[#d8f7ef33]" />
        <p
          className="mt-4 cursor-pointer pb-6 text-base font-medium leading-5.6 text-primary-white"
          onClick={() => setActiveFilters([])}
        >
          {clearButton}
        </p>
      </div>
    </div>
  )
}

type APIsFilterProps = {
  allTags: string[]
  clearButton: string
  activeFilters: string[]
  setActiveFilters: (filters: string[]) => void
  firstTag: string
  filter: { label: string; image: string }
}

function APIsFilter(props: APIsFilterProps) {
  const {
    allTags,
    clearButton,
    activeFilters,
    setActiveFilters,
    firstTag,
    filter,
  } = props

  const [isFilterOpen, setIsFilterOpen] = useState<boolean>(false)

  const apisFilterButtonProps = {
    filter,
    isFilterOpen,
    setIsFilterOpen,
  }

  return (
    <div className={"mb-[3px] md:mb-0 " + (isFilterOpen ? '' : 'flex')}>
      {isFilterOpen ? (
        <div className="self-end">
          <FilterDropDown
            apisFilterButtonProps={apisFilterButtonProps}
            activeFilters={activeFilters}
            setActiveFilters={setActiveFilters}
            firstTag={firstTag}
            clearButton={clearButton}
            allTags={allTags}
            setIsFilterOpen={setIsFilterOpen}
          />
        </div>
      ) : (
        <div className="self-end">
          <FilterButton
            filter={filter}
            isFilterOpen={isFilterOpen}
            setIsFilterOpen={setIsFilterOpen}
          />
        </div>
      )}
    </div>
  )
}

const memorizedAPIsFilter = React.memo(APIsFilter)

export default memorizedAPIsFilter
