import { useState } from 'react'

type SearchBarProps = {
  searchPlaceholder: string
  apis: {
    image: string
    heading: string
    description: string
    button: { label: string; link: string }
  }[]
}

export default function SearchBar(props: SearchBarProps) {
  const { searchPlaceholder, apis } = props

  const [isFocused, setIsFocused] = useState<boolean>(false)
  const [searchBarValue, setSearchBarValue] = useState<string>('')

  const apisFromSearch = apis.filter((api) =>
    api.heading.toLowerCase().includes(searchBarValue.toLowerCase())
  )

  const isSearchValid =
    searchBarValue.length > 0 &&
    searchBarValue !== ' ' &&
    apisFromSearch.length > 0

  return (
    <div className="relative mb-6 flex w-full flex-col flex-wrap md:max-w-[443px]">
      <input
        placeholder={searchPlaceholder}
        type="text"
        value={searchBarValue}
        onChange={(e) => setSearchBarValue(e.target.value)}
        onFocus={() => setIsFocused(true)}
        onBlur={() => (setSearchBarValue(''), setIsFocused(false))}
        className={
          `
          h-[58px] w-full
          rounded-lg border-2
          border-[#FFFFFF29] bg-transparent
          bg-[center_left_16px] bg-no-repeat
          py-[18px] pl-6
          text-base leading-5.6
          text-[#969AA6] outline-none
          transition focus:bg-primary-white
          focus:bg-search focus:pl-14
          focus:text-primary-black
        ` + (isSearchValid ? ` rounded-b-none` : '')
        }
      />
      {isFocused ? (
        <div
          className="
              absolute mr-4 
              mt-[18px] h-6 
              w-6 self-end
              bg-cross bg-center
              bg-no-repeat md:mt-3
              lg:mt-4
            "
          onClick={() => setSearchBarValue('')}
        />
      ) : (
        <></>
      )}
      {isSearchValid ? (
        <div
          className="
              absolute mt-[58px] 
              flex w-full flex-col
              rounded-b-lg border-x-4
              border-primary-white bg-primary-white
              px-4 md:mt-12 lg:mt-[57px]
            "
        >
          {apisFromSearch.map((api, i) => (
            <a
              href={api.button.link}
              key={'sb' + i + api.heading}
              className="
                    h-[58px] w-full
                    border-t bg-primary-white
                    py-[18px] pl-10
                    text-base leading-5.6
                    text-primary-black
                  "
            >
              {api.heading}
            </a>
          ))}
        </div>
      ) : (
        <></>
      )}
    </div>
  )
}
