import React, { useState } from 'react'
import { sectionMarginBottom } from '../../../../utils/classNames'

import arrowDown from '../../../assets/svg/arrow-down-emerald.svg'
import arrowUp from '../../../assets/svg/arrow-up-emerald.svg'

export interface Props {
  questions: { question: string; answer: string }[]
  header?: React.ReactNode
}

interface AccordionItemProps {
  question: string
  answer: string
  isOpen: boolean
  index: number
  onQuestionClick: (index: number) => void
}

const AccordionItem = ({
  question,
  answer,
  isOpen,
  index,
  onQuestionClick,
}: AccordionItemProps) => (
  <div
    className="mx-auto mb-8 max-w-[700px] cursor-pointer border-b border-b-[#D8F7EF] border-opacity-20"
    onClick={() => onQuestionClick(index)}
  >
    <div className="mb-8 flex justify-between">
      <div className="text-xl font-bold text-secondary-gray">{question}</div>
      <div className="h-6 w-6">
        <img src={isOpen ? arrowUp : arrowDown} className="w-full" />
      </div>
    </div>
    {isOpen && <div className="mb-8 text-xl text-tertiary-black">{answer}</div>}
  </div>
)

const FAQ = ({ questions, header }: Props) => {
  const [openQuestionIndex, setOpenQuestionIndex] = useState<number | null>(0)

  const handleQuestionClick = (index: number) => {
    if (openQuestionIndex === index) {
      setOpenQuestionIndex(null)
    } else {
      setOpenQuestionIndex(index)
    }
  }

  return (
    <section className={sectionMarginBottom}>
      <div>{header}</div>
      <div>
        {questions.map((question, index) => (
          <AccordionItem
            {...question}
            key={question.question}
            index={index}
            isOpen={openQuestionIndex === index}
            onQuestionClick={handleQuestionClick}
          />
        ))}
      </div>
    </section>
  )
}

export default FAQ
