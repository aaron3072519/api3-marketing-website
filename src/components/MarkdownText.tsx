import classNames from 'classnames'

export interface Props {
  text: string
  className?: string
  linkClassName?: string
}

export const MarkdownText = ({ text, className, linkClassName }: Props) => {
  const parseText = (text: string, className?: string) => {
    const markdown = /__(.*?)__|\[(.*?)\]\((.*?)\)/g
    const parts = []

    let lastIndex = 0
    let match

    while ((match = markdown.exec(text)) !== null) {
      const [matchedText, boldContent, linkText, linkUrl] = match
      const index = match.index

      if (index > lastIndex) {
        parts.push(text.slice(lastIndex, index))
      }

      if (boldContent) {
        parts.push(
          <span key={index} className={classNames(`font-bold`, className)}>
            {boldContent}
          </span>
        )
      } else if (linkText && linkUrl) {
        parts.push(
          <a
            className={classNames(`font-bold`, linkClassName)}
            key={index}
            href={linkUrl}
          >
            {linkText}
          </a>
        )
      }

      lastIndex = index + matchedText.length
    }

    if (lastIndex < text.length) {
      parts.push(text.slice(lastIndex))
    }

    return <>{parts}</>
  }

  return <>{parseText(text, className)}</>
}
