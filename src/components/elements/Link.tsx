interface Props {
  label: string
  link: string
  color?: string
  isExternal?: boolean
}

const Link = ({
  label,
  link,
  color = 'emerald',
  isExternal = false,
}: Props) => {
  const decors =
    color === 'emerald'
      ? 'hover:bg-link_light_emerald_arrow active:bg-link_emerald_arrow'
      : color === 'blue'
      ? 'hover:bg-link_light_blue_arrow active:bg-link_blue_arrow'
      : color === 'purple'
      ? 'hover:bg-link_light_purple_arrow active:bg-link_purple_arrow'
      : ''

  return (
    <a href={link} target={isExternal && '_blank'}>
      <button
        className={`
        font-medium button-${color}
        relative bg-[center_right]
        bg-no-repeat hover:pr-5
        active:pr-7
        ${decors}
      `}
      >
        {label}
      </button>
    </a>
  )
}

export default Link
