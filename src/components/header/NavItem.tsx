import classNames from 'classnames'
import { useState, useRef } from 'react'

import SubmenuItem from './SubmenuItem'

const NavItem = ({
  title,
  navItems,
  setActiveSubmenu,
  activeSubmenu,
  isLastItem,
}) => {
  const [activeSubcategory, setActiveSubcategory] = useState(null)
  const submenuRef = useRef<HTMLDivElement>(null)

  const isSubmenuVisible = title === activeSubmenu

  const toggleSubmenu = () => {
    setActiveSubmenu(isSubmenuVisible ? null : title)
  }

  const handleSubmenuClick = (label: string, hasSubcategories: boolean) => {
    if (hasSubcategories) {
      setActiveSubcategory((prev: string) => (prev === label ? null : label))
    }
  }

  const renderSubcategories = (
    subcategories: {
      link: string
      label: string
      icon: string
      isExternal?: boolean
    }[]
  ) => {
    return (
      <div className="absolute left-full top-0 flex h-full min-w-max flex-col gap-6 rounded-r-[10px] bg-[#181D2C] p-10">
        {subcategories.map(
          (subcategory: {
            link: string
            label: string
            icon: string
            isExternal?: boolean
          }) => (
            <a
              href={subcategory.link}
              className="block"
              key={subcategory.label}
              target={subcategory.isExternal && '_blank'}
            >
              <div className="flex items-center">
                <div className="mr-4 h-6 w-6 rounded-sm bg-[#2B303D] py-[5px] px-1">
                  <img src={subcategory.icon} />
                </div>
                <div className="text-sm text-primary-white hover:text-primary-emerald">
                  {subcategory.label}
                </div>
              </div>
            </a>
          )
        )}
      </div>
    )
  }

  return (
    <div className="relative shrink-0">
      <div
        className="group relative flex cursor-pointer items-center text-primary-white"
        onClick={toggleSubmenu}
      >
        <div className="mr-3 font-medium capitalize group-hover:text-primary-emerald">
          {title}
        </div>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="11"
          height="7"
          viewBox="0 0 11 7"
          fill="none"
          className="stroke-[#FCFCFF] group-hover:stroke-primary-emerald"
        >
          <path
            d="M1.60529 1L5.60529 5L9.60529 1"
            strokeWidth="2"
            strokeLinecap="round"
            fill="none"
          ></path>
        </svg>
      </div>
      {isSubmenuVisible && (
        <div
          className={classNames(
            'absolute mt-[19px] -ml-[40px] flex min-w-max flex-col gap-6 bg-[#181D2C] p-10',
            activeSubcategory !== null && isLastItem
              ? 'ml-[-400%] rounded-l-[10px]'
              : activeSubcategory !== null && !isLastItem
              ? 'ml-[-340%] rounded-l-[10px]'
              : 'rounded-[10px]'
          )}
          ref={submenuRef}
        >
          {navItems.map((item: { label: string }) => (
            <SubmenuItem
              key={item.label}
              item={item}
              isActive={activeSubcategory === item.label}
              handleClick={handleSubmenuClick}
              renderSubcategories={renderSubcategories}
            />
          ))}
        </div>
      )}
    </div>
  )
}

export default NavItem
