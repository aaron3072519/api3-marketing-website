import classNames from 'classnames'
import { useState, useRef, useEffect } from 'react'
import { useOnClickOutside } from '../hooks/useOnClickOutside'

import NavItem from './NavItem'
import NavItemMobile from './NavItemMobile'
import SubmenuItemMobile from './SubmenuItemMobile'

import logo from '../../assets/images/api3-logo.png'
import menuIcon from '../../assets/svg/menu-icon.svg'
import menuCloseIcon from '../../assets/svg/menu-close-icon.svg'
import arrowLeft from '../../assets/svg/arrow-left.svg'

import { data } from '../../../data/header'

const Header = () => {
  const ref = useRef()
  const [activeSubmenu, setActiveSubmenu] = useState(null)
  const [activeSubmenuMobile, setActiveSubmenuMobile] = useState(null)
  const [activeNavItem, setActiveNavItem] = useState(null)
  const [isSubcategory, setIsSubcategory] = useState(false)
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false)
  useOnClickOutside(ref, () => setActiveSubmenu(null))

  const openMobileMenu = () => {
    setIsMobileMenuOpen(true)
    document.body.style.overflow = 'hidden'
  }

  const closeMobileMenu = () => {
    setIsMobileMenuOpen(false)
    setActiveSubmenuMobile(null)
    setActiveNavItem(null)
    setIsSubcategory(false)
    document.body.style.overflow = 'unset'
  }

  useEffect(() => {
    window.addEventListener('resize', closeMobileMenu)

    return () => window.removeEventListener('resize', closeMobileMenu)
  }, [])

  const openSubmenuMobile = (navItem) => {
    setActiveSubmenuMobile(navItem.navItems)
    setActiveNavItem(navItem.title)
  }

  const goBackInMobileMenu = (menuItem) => {
    const newActiveSubmenuMobile = data.menu.find(
      (item) => item.title === menuItem
    )

    if (newActiveSubmenuMobile) {
      setActiveSubmenuMobile(null)
      setActiveNavItem(null)
      setIsSubcategory(false)
    } else {
      const newSubcategory = data.menu.find((item) => {
        return item.navItems.find(
          (subItem: { label: string }) => subItem.label === menuItem
        )
      })
      setActiveSubmenuMobile(newSubcategory.navItems)
      setActiveNavItem(newSubcategory.title)
      setIsSubcategory(false)
    }
  }

  const openSubcategories = (item) => () => {
    setActiveNavItem(item.label)
    setActiveSubmenuMobile(item.subcategories)
    setIsSubcategory(true)
  }

  return (
    <nav className="container pt-5 lg:pt-16" ref={ref}>
      <div className="flex items-center justify-between">
        {activeNavItem ? (
          <div
            className="flex cursor-pointer items-center"
            onClick={() => goBackInMobileMenu(activeNavItem)}
          >
            <img src={arrowLeft} className="mr-[22px]" />
            <div className="text-xl font-bold text-primary-white md:text-[22px] md:text-1xl">
              {activeNavItem}
            </div>
          </div>
        ) : (
          <a href="/">
            <img src={logo} alt="API3" className="w-[77px] lg:w-[142.17px]" />
          </a>
        )}
        <div className="hidden gap-10 lg:flex">
          {data.menu.map(
            (menuItem: { title: string; navItems }, _i: number, arr) => (
              <NavItem
                key={menuItem.title}
                title={menuItem.title}
                navItems={menuItem.navItems}
                setActiveSubmenu={setActiveSubmenu}
                activeSubmenu={activeSubmenu}
                isLastItem={arr.length - 1 === _i}
              />
            )
          )}
        </div>
        <div className="lg:hidden">
          <img
            src={isMobileMenuOpen ? menuCloseIcon : menuIcon}
            className="w-[22px] cursor-pointer lg:w-8"
            onClick={isMobileMenuOpen ? closeMobileMenu : openMobileMenu}
          />
        </div>
      </div>
      <div
        className={classNames(
          'mt-5 h-screen bg-[#030819] md:mt-8 lg:hidden',
          isMobileMenuOpen ? 'block' : 'hidden'
        )}
      >
        {activeSubmenuMobile
          ? activeSubmenuMobile.map(
              (submenu: {
                label: string
                description?: string
                link?: string
                icon?: string
                subcategories?: { label: string; link: string; icon: string }[]
              }) => (
                <SubmenuItemMobile
                  item={submenu}
                  key={submenu.label}
                  openSubcategories={openSubcategories}
                  isSubcategory={isSubcategory}
                />
              )
            )
          : data.menu.map((menuItem) => (
              <NavItemMobile
                navItem={menuItem}
                key={menuItem.title}
                onClick={openSubmenuMobile}
              />
            ))}
      </div>
    </nav>
  )
}

export default Header
