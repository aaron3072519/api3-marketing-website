const IsLink = ({ href, isExternal = false, children }) => {
  if (href) {
    return (
      <a
        href={href}
        target={isExternal && '_blank'}
        className="group flex cursor-pointer items-center"
      >
        {children}
      </a>
    )
  }

  return (
    <div className="group flex cursor-pointer items-center">{children}</div>
  )
}

const SubmenuItem = ({ item, isActive, handleClick, renderSubcategories }) => {
  return (
    <div
      className="flex"
      {...(!!item.subcategories
        ? { onClick: () => handleClick(item.label, true) }
        : {})}
    >
      <IsLink
        href={!item.subcategories ? item.link : false}
        isExternal={item.isExternal}
      >
        <img className="mr-4 h-14 w-14" src={item.icon} />
        <div>
          <div className="flex flex-nowrap items-center gap-3 font-medium text-primary-white group-hover:text-primary-emerald">
            {item.label}
            {item.subcategories && (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="12"
                height="12"
                viewBox="0 0 12 12"
                fill="none"
                className="fill-primary-white group-hover:fill-primary-emerald"
              >
                <path d="M6.392 11.301L5.37 10.29 8.92 6.739H0V5.26h8.92L5.37 1.716 6.391.699 11.693 6l-5.3 5.301z"></path>
              </svg>
            )}
          </div>
          {item.description && (
            <div className="text-sm text-tertiary-black3 group-hover:text-primary-white">
              {item.description}
            </div>
          )}
        </div>
      </IsLink>
      {isActive && item.subcategories && (
        <div className="flex">
          <div className="absolute right-0 top-[40px] h-[calc(100%-80px)] w-px bg-[#2B303D]" />
          {renderSubcategories(item.subcategories)}
        </div>
      )}
    </div>
  )
}

export default SubmenuItem
