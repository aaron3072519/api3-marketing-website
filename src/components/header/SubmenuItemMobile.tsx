import classNames from 'classnames'

import arrowRight from '../../assets/svg/arrow-right.svg'

interface SubmenuItemMobileProps {
  item: {
    label: string
    description?: string
    link?: string
    icon?: string
    isExternal?: boolean
    subcategories?: {
      label: string
      link: string
      icon: string
      isExternal?: boolean
    }[]
  }
  isSubcategory?: boolean
  openSubcategories: (item: {
    label: string
    description?: string
    link?: string
    icon?: string
    isExternal?: boolean
    subcategories?: {
      label: string
      link: string
      icon: string
      isExternal?: boolean
    }[]
  }) => () => void
}

const SubmenuItemMobile = ({
  item,
  isSubcategory = false,
  openSubcategories,
}: SubmenuItemMobileProps) => {
  return (
    <a href={item.link} className="block">
      <div
        className={classNames(
          'full-width items-center border-b border-solid border-b-[#D8F7EF1A] border-opacity-10 bg-[#181D2C]',
          item.subcategories && 'cursor-pointer'
        )}
        onClick={item.subcategories && openSubcategories(item)}
      >
        <div className="flex items-center gap-4 py-5 px-[30px] last-of-type:border-b-0 md:py-6 md:px-14">
          <img
            src={item.icon}
            className={classNames(isSubcategory ? 'w-6' : 'w-14')}
          />
          <div>
            <div
              className={classNames(
                'capitalize text-primary-white',
                isSubcategory ? 'font-normal' : 'text-base font-bold md:text-xl'
              )}
            >
              {item.label}
            </div>
            <div className="mt-1 text-sm text-tertiary-black3">
              {item.description}
            </div>
          </div>
          {item.subcategories && <img src={arrowRight} className="ml-auto" />}
        </div>
      </div>
    </a>
  )
}

export default SubmenuItemMobile
