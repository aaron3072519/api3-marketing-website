import arrowRight from '../../assets/svg/arrow-right.svg'

interface NavItemMobileProps {
  navItem: {
    title: string
    navItems: {
      label: string
      description?: string
      link?: string
      icon?: string
    }[]
  }
  onClick: (navItem) => void
}

const NavItemMobile = ({ navItem, onClick }: NavItemMobileProps) => {
  const handleOpenSubmenu = () => {
    onClick(navItem)
  }

  return (
    <div className="full-width cursor-pointer border-b border-solid border-b-[#D8F7EF1A] border-opacity-10 bg-[#181D2C]">
      <div
        className="flex items-center justify-between py-[25px] px-[30px] md:px-14"
        onClick={handleOpenSubmenu}
      >
        <div className="text-base font-bold capitalize text-primary-white md:text-xl">
          {navItem.title}
        </div>
        <img src={arrowRight} />
      </div>
    </div>
  )
}

export default NavItemMobile
