export const textColor = {
  emerald: 'text-tertiary-emerald',
  blue: 'text-tertiary-blue',
  purple: 'text-tertiary-purple',
  white: 'text-tertiary-black',
}

export const backgroundColor = {
  green: 'bg-green-rgba',
  blue: 'bg-blue-rgba',
  violet: 'bg-violet-rgba',
  gray: 'bg-gray-rgba',
}

export const backgroundGradient = {
  emerald: 'background-gradient-emerald border-mask-gradient-emerald before:rounded-2xl',
  blue: 'background-gradient-blue border-mask-gradient-blue before:rounded-2xl',
  purple: 'background-gradient-purple border-mask-gradient-purple before:rounded-2xl',
}

export const sectionMarginBottom = 'mb-30 md:mb-48 lg:mb-75'
